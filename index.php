<!doctype html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Is Github Being DDOS'd?</title>
  <style>
    h1 {
      font-family: sans-serif;
      font-size: 70px;
      width: 100%;
      text-align: center;
    }
  </style>
</head>
<body>
  <h1>
    <?php
      $ch = curl_init("https://status.github.com/api/status.json");
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      $res = curl_exec($ch);
      curl_close($ch);
      
      $json_decoded = json_decode($res);
      
      if ($json_decoded->status === "good") {
        echo "No!";
      } else {
        echo "Yes!";
      }
    ?>
  </h1>
</body>
</html>